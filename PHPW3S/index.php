<!DOCTYPE html>
<html>
<head>
	<title>PHP in W3S</title>
	<link rel="stylesheet" type="text/css" href="/home/car/Documentos/Otros/PrgrTt/Web/CSS/W3S/styles.css">
</head>
<body>
	<h2>What Can PHP Do?</h2>
	<?php
	$cars = array("Volvo", "BMW", "Toyota");
	$arrlength = count($cars);

	echo ("El arreglo tiene: $arrlength elementos <br>");

	for($x = 0; $x < $arrlength; $x++) {
	    echo $cars[$x];
	    echo "<br>";
	}

	$age = array("Peter"=>"35", "Ben"=>"37", "Joe"=>"43");
	echo "Ben is " . $age['Ben'] . " years old." . "<br>";
	foreach ($age as $x => $x_value) {
		echo "Key=" . $x . ", Value=" . $x_value;
		echo "<br>";
	}
	ksort($age);
	echo "<br>";
	foreach ($age as $x => $x_value) {
		echo "Key=" . $x . ", Value=" . $x_value;
		echo "<br>";
	}

	echo $_SERVER['PHP_SELF'];
	echo "<br>";
	echo $_SERVER['GATEWAY_INTERFACE'];
	echo "<br>";
	echo $_SERVER['HTTP_HOST'];
	echo "<br>";
	echo $_SERVER['HTTP_REFERER'];
	echo "<br>";
	echo $_SERVER['HTTP_USER_AGENT'];
	echo "<br>";
	echo $_SERVER['SCRIPT_NAME'];
	?>
</body>
</html>