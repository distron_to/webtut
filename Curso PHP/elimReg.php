<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Inserción</title>
    <link rel="stylesheet" href="tablas.css">

</head>
<body>
<?php

$id = $_GET["idArt"];
$sec = $_GET["seccion"];
$nom = $_GET["n_art"];
$pre = $_GET["precio"];
$fec = $_GET["fecha"];
$por = $_GET["p_orig"];

require 'ConexionBBDD.php';
$conexion = mysqli_connect($dbHost, $dbUsuario, $dbPass);

if (mysqli_connect_errno()) {
    echo "Fallo al conectar con la BBDD";
    exit();
}
mysqli_select_db($conexion, $dbNombre) or die("No se encuentra la BBDD");
mysqli_set_charset($conexion, "utf8");

$query = "DELETE FROM Articulos WHERE idArticulo = '$id'";
$resultado = mysqli_query($conexion, $query);

if($resultado==false){
    echo 'Error en la consulta';
}
else{
    //echo 'Registro eliminado<br><br>';
    //echo mysqli_affected_rows($conexion);
    if(mysqli_affected_rows($conexion)==0){
        echo "No hay elementos a eliminar";
    }
    elseif(mysqli_affected_rows($conexion)==1){
        echo "Se ha eliminado " . mysqli_affected_rows($conexion) . " registro.";
    }
    else{
        echo "Se han eliminado " . mysqli_affected_rows($conexion) . " registros.";
    }
}

mysqli_close($conexion);

?>
</body>
</html>