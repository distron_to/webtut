<?php 
	/**
	 * 
	 */
	class Coche	{
	    protected $ruedas;
		var $color;
		protected $motor;
		function Coche(){
			$this->ruedas=4;
			$this->color="";
			$this->motor=1600;
		}
		function getRuedas(){
			return $this->ruedas;
		}
        function getMotor(){
            return $this->motor;
        }
		function arrancar(){
			echo "Estoy arrancando<br>";
		}
		function girar(){
			echo "Estoy girando<br>";
		}
		function frenar(){
			echo "Estoy frenando<br>";
		}
		function setColor($colorCoche, $nomCoche){
			$this->color=$colorCoche;
			echo "El color de el ". $nomCoche . " es " . $this->color . "<br>";
		}
	}

	//--------------------------------------------------------------------------------------------------------

	class Camion extends Coche{

		function Camion(){
			$this->ruedas=8;
			$this->color="";
			$this->motor=2600;
		}
		function setColor($colorCamion, $nomCamion){
			$this->color=$colorCamion;
			echo "El color de el ". $nomCamion . " es " . $this->color . "<br>";
		}
		function arrancar(){
			parent::arrancar();
			echo "Camion arrancado";
		}
	}
?>