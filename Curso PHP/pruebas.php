<!DOCTYPE html>
<html>
<head>
	<title>PHP PI</title>
    <link rel="stylesheet" href="tablas.css">
</head>
<body>
<?php 
    require 'ConexionBBDD.php';
	$conexion = mysqli_connect($dbHost,$dbUsuario,$dbPass);

	if(mysqli_connect_errno()){
	    echo "Fallo al conectar con la BBDD";
	    exit();
    }
	mysqli_select_db($conexion,$dbNombre) or die("No se encuentra la BBDD");
	mysqli_set_charset($conexion, "utf8");

	$query = "SELECT * FROM Articulos WHERE PAISORIGEN = 'China'";
    $resultado = mysqli_query($conexion, $query);

    while ($fila=mysqli_fetch_array($resultado, MYSQLI_ASSOC)){

        echo "<table><tr><td class='idArticulo'>";
        echo $fila['idArticulo']."</td><td>";
        echo $fila['seccion']."</td><td>";
        echo $fila['nombreArticulo']."</td><td>";
        echo $fila['fecha']."</td><td>";
        echo $fila['paisOrigen']."</td></tr></table>";
        echo "<br>";

    }
    mysqli_close($conexion);

?>
</body>
</html>