<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" name="keywords" content="HTML, CSS, XML, JavaScript">
	<title></title>
</head>
<body>

	<h2 id="JS">What Can JavaScript Do?</h2>
		<p id="demo">JavaScript can change HTML content.</p>
		<button type="button" onclick='document.getElementById("demo").innerHTML="Hello JavaScript!"'>Click Me!</button>
		<button type="button" onclick="document.getElementById('demo').innerHTML='Hello JS!'">Click Me!</button>

		<p>JavaScript can change HTML attribute values.</p>
		<p>In this case JavaScript change the value of the src (source) attrib</p>

		<img src="/var/www/html/Curso PHP/imagenes/gato.jpg" alt="x">
		<br>

<?php 
	//require ("funcion.php");
	define("GREETING", "Welcome to W3Schools.com!");
	define("cars", [
		"Alfa Romeo",
		"BMW",
		"Toyota"
	]);
	$t = date("H");
	$x = "5435.1321";
	$int_cast = (int)$x;
	$colors = array("red", "blue", "green", "yellow");

	foreach ($colors as $value) {
		echo "$value <br>";
	}
	
	echo cars[2];
	echo "<br>";
	var_dump(is_numeric($x));
	echo "<br>";
	echo $t;
	echo "<br>";
	myTest();
	echo "<br>";

	if ($t < "20") {
		echo "Have a good day!";
	}
	elseif ($t == "20") {
		echo "Have a good night";
	}
	else{
		echo "Have a good n";
	}
	echo "<br>";
	
	class Car{
		function Car(){
			$this->model = "VW";
		}
	}
	//create an object
	$herbie = new Car();
	// show object properties
	echo $herbie->model;
	echo "<br>";

?>

</body>
</html>