'use strict'

/*
1. Pida 6 numeros por pantalla y los meta en un array
2. Mostrar el array entero (todos sus elementos) en
el cuerpo de la pagina y en la consola
3. Ordenarlo y mostrarlo
4. Invertir su orden y mostrarlo
5. Mostrar cuantos elementos tiene el array
6. Busqueda de un valor introducido por el usuario,
que nos diga si lo encuentra y su indice 
(Se valorará el  uso de funciones)
*/
var numeros = [];
var i, x, num;

for(i=0; i<6; i++){
	numeros.push( parseInt( prompt("Introduce un numero", 0) ) );
}
//Muestra en el cuerpo
imprimir(numeros);

//Muestra en la consola
console.log(numeros);

//Ordena y muestra
numeros.sort( function(a, b){return a-b} );
imprimir(numeros, 'ordenado');

//Invierte y muestra
numeros.reverse();
imprimir(numeros, 'invertidos');

//Elementos
document.write("<h2>El array tiene "+numeros.length+" elementos</h2>");

//Busqueda
num = parseInt(prompt("Introduce numero a buscar", 0));
busqueda(num, numeros);

function imprimir(elementos, textoCustom=""){
	document.write("<h1>"+textoCustom+"</h1>");
	document.write("<ul>");
	elementos.forEach( (elemento, index) => {
		document.write("<li>"+elemento+"</li>");
	});
	document.write("</ul>");
}

function busqueda(num, arreglo){
	var num2=0;
	for(i=0; i<6; i++){
		if(num!=arreglo[i]){
			document.write("No existe");
		}
		else{
			document.write("Su posicion es: "+i);
		}
	}
}