'use strict'

/*
FUNCIONES
Una funcion es una agrupacion reutilizable
de un conjunto de instrucciones
*/

function porConsola(numero1, numero2){
	console.log("Suma: " +(numero1+numero2));
	console.log("Resta: " +(numero1-numero2));
	console.log("Multi: " +(numero1*numero2));
	console.log("Divi: " +(numero1/numero2));
	console.log("****************************");
}

function porPantalla(numero1, numero2){
	document.write("Suma: " +(numero1+numero2)+"</br>");
	document.write("Resta: " +(numero1-numero2)+"</br>");
	document.write("Multi: " +(numero1*numero2)+"</br>");
	document.write("Divi: " +(numero1/numero2)+"</br>");
	document.write("****************************"+"</br>");
}

function calculadora(numero1, numero2, mostrar=false){
	if(mostrar==false){
		porConsola(numero1, numero2);
	}
	else{
		porPantalla(numero1, numero2);
	}
	return true;
}

//calculadora(1,4);
calculadora(1,4,true);
//calculadora(3,6,true);