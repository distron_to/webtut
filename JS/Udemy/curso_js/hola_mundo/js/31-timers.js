'use strict'

window.addEventListener('load', function () {
    //TIMERS
    function intervalo() {
        var tiempo = setInterval(function () {
            console.log('Set interval ejecutado');
            var encabezado = document.querySelector("h2");
            if (encabezado.style.fontSize == "40px"){
                encabezado.style.fontSize = "20px";
            }
            else {
                encabezado.style.fontSize = "40px";
            }
        }, 500);
        return tiempo;
    }
    var tiempo = intervalo();
    var stop = document.querySelector("#stop");
    stop.addEventListener("click", function () {
        alert('Has parado el  bucle');
        clearInterval(tiempo);
    });

    var start = document.querySelector("#start");
    start.addEventListener("click", function () {
        alert('Has iniciado el  bucle');
        intervalo();
    });

});