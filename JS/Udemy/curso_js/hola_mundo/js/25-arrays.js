'use strict'

var nombres = ["Carlos Molina", "Victor Robles", "Juan Gutiérrez", 52, true];
var lenguajes = new Array("PHP", "JS", "Go", "Java", "C");

console.log(nombres);
console.log(lenguajes);
console.log(nombres.length);
/*
var elemento = parseInt(prompt("Que elemento del array quieres?", 0));
if(elemento>= nombres.length){
    alert("Introduce el numero correcto menor que "+nombres.length);
}else{
    alert("El usuario seleccionado es "+nombres[elemento]);
}
*/
document.write("<h1>Lenguajes de programacion del 2018</h1>");
document.write("<ul>");
/*
for(var i=0; i<lenguajes.length; i++){
    document.write("<li>"+lenguajes[i]+"</li>");
}
*/
lenguajes.forEach( (elemento, index, data)=>{
	console.log(data);
	document.write("<li>"+index+" "+elemento+"</li>");
});
document.write("</ul>");