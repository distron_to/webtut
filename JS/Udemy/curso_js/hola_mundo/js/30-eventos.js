'use strict'

//EVENTOS DEL RATÓN
window.addEventListener('load', function () {
    function cambiaColor() {
        console.log("picaste");
        var bg = boton.style.background;
        if(bg == "#000"){
            boton.style.background = "#faf";
        }
        else{
            boton.style.background = "#0a0";
        }
        return true;
    }
    var boton = document.querySelector("#boton");

//Onclick
    boton.addEventListener('click', function () {
        cambiaColor();
        boton.style.border = "10px solid black";
    });

    var input = document.querySelector("#campo_nombre");
//Focus
    input.addEventListener('focus', function () {
        console.log('[focus]Dentro de input');
    });

//Blur
    input.addEventListener('blur', function () {
        console.log('[blur]Fuera de input');
    });

//keydown
    input.addEventListener('keydown', function (event) {
        console.log('[keydown]Pulsando esta tecla: ', String.fromCharCode(event.keyCode));
    });

//keypress
    input.addEventListener('keypress', function (event) {
        console.log('[keypress]Tecla presionda: ', String.fromCharCode(event.keyCode));
    });

//keyup
    input.addEventListener('keyup', function (event) {
        console.log('[keyup]Tecla soltada: ', String.fromCharCode(event.keyCode));
    });
});//Fin de load

