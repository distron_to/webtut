'use strict'

//DOM Document Object Model

function cambiaColor (color) {
	caja.style.background = color;
}

/*
* CONSEGUIR ELEMENTOS CON ID ESPECIFICO
* getElementById() -> MÉTODO
* innerHTML = ""; -> PROPIEDAD
*/

//var caja = document.getElementById("miCaja");
var caja = document.querySelector("#miCaja");

caja.innerHTML = "¡TEXTO EN LA CAJA DESDE JS!";
caja.style.background = '#66ccff';
caja.style.padding = '20px';
caja.style.color = 'white';

console.log(caja);


/* 
* CONSEGUIR ELEMENTOS POR SU ETIQUETA
*/
var todosLosDivs = document.getElementsByTagName("div");
console.log(todosLosDivs);

/*
var contenido = todosLosDivs[2];
contenido.innerHTML='Otro contenido';
contenido.style.background = 'black';
contenido.style.color = 'white';
console.log(contenido);
*/

var valor;
for(valor in todosLosDivs){
	if(valor < todosLosDivs.length){
		var parrafo = document.createElement("P");
		var texto = document.createTextNode(todosLosDivs[valor].textContent);
		parrafo.appendChild(texto);
		document.querySelector("#miSeccion").appendChild(parrafo);
	}
}

/* 
* CONSEGUIR ELEMENTOS POR SU CLASE CSS
*/
var divsRojos = document.getElementsByClassName('rojo');
var divsAmarillos = document.getElementsByClassName('amarillo');

divsAmarillos[0].style.background = 'yellow';
for(var i in divsRojos){
	if(divsRojos[i].className == 'rojo'){
		divsRojos[i].style.background = 'red';
	}
}

//Query Selector
var id = document.querySelector("#encabezado");
console.log(id);

var claseRojo = document.querySelector("div.rojo");
console.log(claseRojo);
