'use strict';

//Comprobacion de localstorage
if (typeof (Storage) != 'undefined'){
    console.log('LocalStorage disponible');
}
else{
    console.log('LocalStorage NO disponible');
}

//Guardar datos
localStorage.setItem('titulo', 'Curso de Symfony');

//Obtener elemento
document.querySelector('#peliculas').innerHTML = localStorage.getItem('titulo');

//GUARDAR OBJETOS
var usuario = {
    nombre: "Carlos Molina",
    email: "carmol@pm.me",
    web: "carmol.mx"
};

localStorage.setItem('usuario', JSON.stringify(usuario));

//OBTENER OBJETO
var userjs = JSON.parse(localStorage.getItem('usuario'));

console.log(userjs);
document.querySelector('#datos').append(" "+userjs.web + " - " + userjs.nombre);

localStorage.clear();