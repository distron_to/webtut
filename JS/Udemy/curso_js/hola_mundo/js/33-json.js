'use strict';

var pelicula = {
    titulo: 'Barman V Superman',
    año: 2017,
    pais: 'EUA'
};

var peliculas = [
    {titulo: 'Renacido', año: 2017, pais: 'México'},
    pelicula
];

let caja_peliculas = document.querySelector("#peliculas");
let index;
for (index in peliculas){
    let p = document.createElement('p');
    p.append(peliculas[index].titulo + " - " + peliculas[index].año);
    caja_peliculas.append(p);
}
console.log(peliculas);