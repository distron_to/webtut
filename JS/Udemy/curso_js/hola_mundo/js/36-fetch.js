'use strict';

//FETCH (AJAX) Y PETICIONES A SERVICIOS / APIS REST

let div_usuarios = document.querySelector('#usuarios');
let div_janet = document.querySelector('#Janet');
let div_profe = document.querySelector('#profe');

//fetch('https://jsonplaceholder.typicode.com/users')
getUsuarios()
	.then(data => data.json())
	.then(users => {
		listadoUsuarios(users.data);
		return getInfo();
	})
	.then(data => {
		div_profe.innerHTML = data;
		return getJanet();
	})
	.then(data => data.json())
	.then(user => {
		mostrarJanet(user.data);
	})
	.catch(error => {
		alert('error en las petis');
	});

function getUsuarios(){
	return fetch('https://reqres.in/api/users');
}

function getJanet(){
	return fetch('https://reqres.in/api/users/2');
}

function getInfo(){
	var profesor = {
		nombre: 'Carlos',
		apellidos: 'Molina',
		url: 'https://victorroblesweb.es'
	};
	return new Promise((resolve, reject) => {
		var profe_string = '';
		setTimeout(function(){
			profe_string = JSON.stringify(profesor);
			if(typeof profe_string != 'string' || profe_string == '')
				return reject('error de nosotros');
			return resolve(profe_string);
		}, 3000);

		
	});
}

function listadoUsuarios (usuarios) {
	usuarios.map((user, i) => {
			let nombre = document.createElement('h3');
			nombre.innerHTML = i + " " + user.first_name + " " + user.last_name;
			div_usuarios.append(nombre);
			document.querySelector(".loading").style.display = 'none';
		});
}

function mostrarJanet (user) {
	let nombre = document.createElement('h4');
	let avatar = document.createElement('img');


	nombre.innerHTML = user.first_name + " " + user.last_name;
	avatar.src = user.avatar;
	avatar.width = '100';

	div_janet.appendChild(nombre);
	div_janet.appendChild(avatar);
	document.querySelector("#Janet .loading").style.display = 'none';
}