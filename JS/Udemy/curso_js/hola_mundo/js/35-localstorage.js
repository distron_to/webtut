'use strict';

var formulario = document.querySelector('#formPeliculas');
var formularioB = document.querySelector('#formBorraPeliculas');

formulario.addEventListener('submit', function () {
    var titulo = document.querySelector('#addPelicula').value;
    if (titulo.length >= 1){
        localStorage.setItem(titulo, titulo);
    }
});

var ul = document.querySelector('#lista');
for (var i in localStorage){
    console.log(localStorage[i]);
    let list = document.createElement("li");
    if(typeof localStorage[i] == 'string'){
        list.append(localStorage[i]);
        ul.append(list);
    }
}

formularioB.addEventListener('submit', function () {
    var titulo = document.querySelector('#borraPelicula').value;
    if (titulo.length >= 1){
        localStorage.removeItem(titulo, titulo);
    }
});