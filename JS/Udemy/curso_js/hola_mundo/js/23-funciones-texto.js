'use strict'

//TRANSFORMACIÓN DE TEXTOS
var numero = 444;
var texto1 = "  Bienvenido al curso de JavaScript curso de Victor Robles    ";
var texto2 = "es muy buen curso";

var dato = numero.toString();
dato = texto1.toLowerCase();
dato = texto2.toUpperCase();
console.log(dato);

//Calcular longitud
var nombre = "Carlos Molina";
nombre = ["hey", "hola"];
console.log(nombre.length);

//Concatenar
/*var textoTotal = texto1+" "+texto2;
console.log(textoTotal);*/

var textoTotal = texto1.concat(" "+texto2);
console.log(textoTotal);
//Encuentra la primer coincidencia
var busqueda = texto1.indexOf("curso");
console.log(busqueda);
//Encuentra la ultima coincidencia
var busqueda = texto1.lastIndexOf("curso");
console.log(busqueda);
//Similar a indexOf()
var busqueda = texto1.search("curso");
console.log(busqueda);
//Busca y muestra en un array
var busqueda = texto1.match("curso");
console.log(busqueda);
//Busca y muestra en un array todas las coincidencias
var busqueda = texto1.match(/curso/g);
console.log(busqueda);
//Busca y muestra a partir del primer dato la longitud del segundo dato
var busqueda = texto1.substr(14,5);
console.log(busqueda);
//Imprime la posicion indicada
var busqueda = texto1.charAt(14);
console.log(busqueda);
//Imprime true o false si empieza con ese texto
var busqueda = texto1.startsWith("Victor");
console.log(busqueda);
//Imprime true o false si termina con ese texto
var busqueda = texto1.endsWith("Victor Robles");
console.log(busqueda);
//Imprime true o false si encuentra ese texto
var busqueda = texto1.includes("Victor");
console.log(busqueda);

//REEMPLAZA TEXTO
var busqueda = texto1.replace("JavaScript", "Symphony");
console.log(busqueda);
//Imprime a partir de la posicion indicada
var busqueda = texto1.slice(14,22);
console.log(busqueda);
//Corta el texto en arrays dependiendo el caracter
var busqueda = texto1.split("e");
console.log(busqueda);
//Corta todos los espacios antes y después del texto
var busqueda = texto1.trim();
console.log(busqueda);